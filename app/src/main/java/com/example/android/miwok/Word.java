package com.example.android.miwok;
import android.media.MediaPlayer;

 import static android.os.Build.VERSION_CODES.M;

/**
 * {@link Word} represents a vocabulary word that the user wants to learn.
 * it contains a default translation and Miwok translation for that word
 */

public class Word {
    //default language word translation in private variable (m- prefix means this variable is private)
    private String mDefaultWord;
    //miwok language word translation in private variable
    private String mMiwokWord;
    //image resource id variable for the word
    private int mResourceId = NO_IMAGE_PROVIDED;

    int mSoundId;



    private static final int NO_IMAGE_PROVIDED = -1;
    //Word class constructor
    //@param defaultWord is the word in a language that user is already familiar with
    //@param miwokWord is the word in a Miwok language
    //@param soundResourceId is the resource ID for the audio file associated with this word
    public Word(String defaultWord, String miwokWord, int soundResourceId) {
        mDefaultWord = defaultWord;
        mMiwokWord = miwokWord;
        mSoundId = soundResourceId;
    }
    //Word class constructor
    //@param defaultWord is the word in a language that user is already familiar with
    //@param miwokWord is the word in a Miwok language
    //@param soundResourceId is the resource ID for the audio file associated with this word
    //@param imageResourceId is the resource ID for the image file associated with this word
    public Word(String defaultWord, String miwokWord, int imageResourceId, int soundResourceId) {
        mDefaultWord = defaultWord;
        mMiwokWord = miwokWord;
        mResourceId = imageResourceId;
        mSoundId = soundResourceId;
    }

    //get the default translation for the word
    public String  getDefaultWord(){
        return mDefaultWord;
    };
    //get the miwok translation for the word
    public String getMiwokWord(){
        return mMiwokWord;
    };
    //get the integer image resource id for the word
    public int getResourceId(){ return mResourceId; };
    //check if word has an image
    public boolean hasImage(){
        return mResourceId != NO_IMAGE_PROVIDED;
    }
    //get the integer sound resource id for the word
    public int soundResourceId(){ return mSoundId; };
}
