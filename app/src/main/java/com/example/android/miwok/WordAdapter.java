package com.example.android.miwok;

import android.app.Activity;
import android.content.Context;

import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ListView;



import java.util.ArrayList;



public class WordAdapter extends ArrayAdapter<Word> {
    //color resource id value
    private int mColorResourceId;




    public WordAdapter(Activity context, ArrayList<Word> words, int colorResourceId) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        //Initialize background color for categories
        super(context, 0, words);

        mColorResourceId = colorResourceId;

    }

    /**
     * Provides a view for an AdapterView (ListView, GridView, etc.)
     *
     * @param position    The position in the list of data that should be displayed in the
     *                    list item view.
     * @param convertView The recycled view to populate.
     * @param parent      The parent ViewGroup that is used for inflation.
     * @return The View for the position in the AdapterView.
     */
;
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate* the view(* - create new list view)
        //it gets null value when you don't have any view to recycle because you just started the app
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Get the {@link Word } object located at this position in the list
        Word currentwords = getItem(position);



        // Find the TextView in the list_item.xml layout with the ID default_word
        TextView defaultTextView = (TextView) listItemView.findViewById(R.id.default_word);
        // Get the version name from the current Word object and
        // set this text on the name TextView
        defaultTextView.setText(currentwords.getDefaultWord());

        // Find the TextView in the list_item.xml layout with the ID version_number
        TextView miwokTextview = (TextView) listItemView.findViewById(R.id.miwok_word);
        // Get the version number from the current Word object and
        // set this text on the number TextView
        miwokTextview.setText(currentwords.getMiwokWord());

        // Find the ImageView in the drawable folder
        ImageView iconView = (ImageView) listItemView.findViewById(R.id.image_word);

        if (currentwords.hasImage()) {
            // Get the image resource ID from the current Word object and
            // set the image to iconView
            iconView.setImageResource(currentwords.getResourceId());
            //make sure the image is visible
            iconView.setVisibility(View.VISIBLE);
        } else {
            //otherwise hide the imageView space
            iconView.setVisibility(View.GONE);
        }

        //Set theme color for the list item
        View textContainer = listItemView.findViewById(R.id.text_container);
        //Find the color id
        int color = ContextCompat.getColor((getContext()), mColorResourceId);
        //Set the background color of the text container view
        textContainer.setBackgroundColor(color);


        // Return the whole list item layout (containing 2 TextViews and an ImageView)
        // so that it can be shown in the ListView
        return listItemView;
    }
}
